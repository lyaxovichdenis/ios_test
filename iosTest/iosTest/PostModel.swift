//
//  PostModel.swift
//  iosTest
//
//  Created by Denis Lyakhovich on 19.12.17.
//  Copyright © 2017 Denis Lyakhovich. All rights reserved.
//

import UIKit
import ObjectMapper

class PostModel: NSObject, Mappable, NSCoding {

    var id: Int?
    var userId: Int?
    var title: String?
    var body: String?
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeInteger(forKey: "id")
        userId = aDecoder.decodeInteger(forKey: "userId")
        title = aDecoder.decodeObject(forKey: "title") as? String
        body = aDecoder.decodeObject(forKey: "body") as? String
    }
    
    func  encode(with aCoder: NSCoder) {
       
        if  let idE = id,
            let userIdE = userId,
            let title = title,
            let body = body {
            
            aCoder.encode(idE, forKey: "id")
            aCoder.encode(userIdE, forKey: "userId")
            aCoder.encode(title , forKey: "title")
            aCoder.encode(body, forKey: "body")
        }

    }
    
    required init?(map: Map) {
        super.init()
        self.mapping(map: map)
    }
    
    func mapping (map: Map) {
        id                    <- map["id"]
        userId                <- map["userId"]
        title                 <- map["title"]
        body                  <- map["body"]
    }
}
