//
//  ArticleDescriptionTableViewCell.swift
//  iosTest
//
//  Created by Denis Lyakhovich on 07.12.17.
//  Copyright © 2017 Denis Lyakhovich. All rights reserved.
//

import UIKit

class ArticleDescriptionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var descriptionText: UILabel!
   
    static let nibName = "ArticleDescriptionTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func  castomize(post: PostModel) {
         descriptionText.text = post.body
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
