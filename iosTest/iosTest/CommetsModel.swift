//
//  CommetsModel.swift
//  iosTest
//
//  Created by Denis Lyakhovich on 27.12.17.
//  Copyright © 2017 Denis Lyakhovich. All rights reserved.
//

import UIKit
import ObjectMapper

class CommetsModel: NSObject, Mappable, NSCoding {
    
    var postId: Int?
    var id: Int?
    var name: String?
    var email: String?
    var body: String?
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        postId = aDecoder.decodeInteger(forKey: "postId")
        id = aDecoder.decodeInteger(forKey: "id")
        name = aDecoder.decodeObject(forKey: "name") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        body = aDecoder.decodeObject(forKey: "body") as? String
    }
    
    func  encode(with aCoder: NSCoder) {
        
        if  let postIdE = postId,
            let idE = id,
            let name = name,
            let email = email,
            let body = body {
            aCoder.encode(postIdE, forKey: "postId")
            aCoder.encode(idE, forKey: "id")
            aCoder.encode(name, forKey: "name")
            aCoder.encode(email, forKey: "email")
            aCoder.encode(body, forKey: "body")
        }
    }
    
    required init?(map: Map) {
        super.init()
        self.mapping(map: map)
    }
    
    
    func mapping (map: Map) {
       
        postId               <- map["postId"]
        id                    <- map["id"]
        name                   <- map["name"]
        email                 <- map["email"]
        body                  <- map["body"]
    }
}
