//
//  BlogsViewController.swift
//  iosTest
//
//  Created by Denis Lyakhovich on 06.12.17.
//  Copyright © 2017 Denis Lyakhovich. All rights reserved.


import UIKit
import Alamofire

class BlogsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PostDataProviderDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var dataProvider: PostsDataProvider? = nil
    
    var refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName:"BlogTableViewCell", bundle: nil), forCellReuseIdentifier: "BlogTableViewCell")
        tableView.register(UINib(nibName:"ArticleDescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "ArticleDescriptionTableViewCell")
        
        dataProvider = PostsDataProvider(d: self)
        dataProvider?.loadPosts()
       
        //refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.tintColor = UIColor.orange
        refreshControl.addTarget(dataProvider, action: #selector(dataProvider?.refresh), for: UIControlEvents.valueChanged)
    
        tableView.addSubview(refreshControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TableView
    
    func  tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if dataProvider == nil {
            return 0
        }
        return dataProvider!.posts.count
   
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var tableViewCell: BlogTableViewCell!
        tableViewCell = tableView.dequeueReusableCell(withIdentifier: "BlogTableViewCell") as!
        BlogTableViewCell
        
        tableViewCell.castomize(post: dataProvider!.posts[indexPath.row]) 
        return tableViewCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 277
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        RouteData.routeArticle(blog: dataProvider!.posts[indexPath.row], fromVC: self)
    }

    // MARK: - BlogsDataProvider
    func postDataLoaded() {
        tableView.reloadData()
        refreshControl.endRefreshing()
        print("Reload posts")
    }
    
    func postDataHasError(error: String) {
    }
}
