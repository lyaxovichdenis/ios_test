//
//  BlogTableViewCell.swift
//  iosTest
//
//  Created by Denis Lyakhovich on 06.12.17.
//  Copyright © 2017 Denis Lyakhovich. All rights reserved.
//

import UIKit

class BlogTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var bodyLabel: UILabel!
    
    func  castomize(post: PostModel) {
        backView.layer.shadowColor = UIColor.black.cgColor
        backView.layer.shadowOpacity = 0.5
        backView.layer.shadowOffset = CGSize.zero
        backView.layer.shadowRadius = 2
        backView.layer.shouldRasterize = true
        
        titleLabel.text = post.title
        bodyLabel.text = post.body
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
