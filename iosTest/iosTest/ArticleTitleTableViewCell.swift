//
//  ArticleTitleTableViewCell.swift
//  iosTest
//
//  Created by Denis Lyakhovich on 07.12.17.
//  Copyright © 2017 Denis Lyakhovich. All rights reserved.
//

import UIKit

class ArticleTitleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleText: UILabel!
    
    static let nibName = "ArticleTitleTableViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func  castomize(post: PostModel) {
        
        titleText.text = post.title
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
