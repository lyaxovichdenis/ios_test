//
//  RouteData.swift
//  iosTest
//
//  Created by Denis Lyakhovich on 16.01.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import UIKit
import Foundation

class RouteData  {
    
    static func routeArticle (blog: PostModel, fromVC: UIViewController) {
        
        let toVc = UIStoryboard(name: "Blogs", bundle: nil).instantiateViewController(withIdentifier: "ArticleViewController") as! ArticleViewController
        toVc.blog = blog
        fromVC.navigationController?.pushViewController(toVc, animated: true)        
    }
}
